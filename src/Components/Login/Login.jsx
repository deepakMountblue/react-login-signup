import React, { Component } from "react";
import './login.css'
import axios from 'axios'
import { Baseurl } from '../utils'
import { Redirect } from "react-router";
export default class Login extends Component {

    state = {
        password: "",
        email: "",
        errMessage: "",
        isDisabled: false,
        login: sessionStorage.getItem('token')?true:false,
        loading: false
    }
    change = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        })
    }
    submit = () => {
        this.setState({ errMessage: "", isDisabled: true })
        axios.post(Baseurl + "login", { ...this.state })
            .then(res => {
                if (res.data.err) {
                    this.setState({ errMessage: res.data.message, isDisabled: false })
                }
                else {
                    sessionStorage.setItem('token', res.data.token)
                    sessionStorage.setItem('email', res.data.email)
                    sessionStorage.setItem('name', res.data.name)
                    this.setState({ login: true })
                }
            }).catch(Err => {
                if (Err.response.data) {
                    this.setState({ errMessage: Err.response.data.message, isDisabled: false })
                }
                else {
                    this.setState({ errMessage: Err.message, isDisabled: false })
                }
            })
    }
    render() {
        if (this.state.login) {
            return (<Redirect to="/" />)
        }
        else if (this.state.loading) {
            return (<div>Loading...</div>)
        }
        else {
            return (
                <div className="form-container">
                    <div className="card">
                        <div className="head">
                            <h2>Login</h2>
                        </div>
                        <div className="head">
                            <p style={{ color: 'red' }}>{this.state.errMessage}</p>
                        </div>
                        <div className="form-level">
                            <label>
                                Email:
                            </label>
                            <input type="email" name="email" onChange={this.change} />
                        </div>
                        <div className="form-level">
                            <label>
                                Password:
                            </label>
                            <input type="password" name="password" onChange={this.change} />
                        </div>
                        <div className="form-level">
                            <button className={this.state.isDisabled ? "normalBtn" : "submitBtn"} onClick={this.submit} disabled={this.state.isDisabled}>Login</button>
                        </div>
                        <div className="form-level">
                            <p>Don't have an account? <a href="/signup">Signup</a> </p>
                        </div>
                    </div>
                </div>
            );
        }

    }
}