import axios from 'axios'
import React, { Component } from 'react'
import { Redirect } from 'react-router'
import { Baseurl } from '../utils'
import './home.css'
import Navbar from '../Navbar/Navbar'
export class Home extends Component {
    state = {
        data: [],
        login: sessionStorage.getItem('token') ? true : false,
        loading: false
    }
    removeSession = () => {
        sessionStorage.removeItem("token")
        sessionStorage.removeItem("name")
        sessionStorage.removeItem("email")
        this.setState({ login: false, loading: false })
    }

    componentDidMount() {
        if (this.state.login) {
            this.setState({ loading: true })
            console.log(sessionStorage.getItem('token'))
            axios.get(Baseurl + '/find', {
                headers: {
                    'x-auth-token': sessionStorage.getItem('token')
                }
            }).then((res) => {
                if (res.data.err) {
                    this.removeSession()
                }
                else {
                    this.setState({ data: res.data, loading: false })
                }
            }).catch(err => {
                console.log(err.response)
                this.removeSession()
            })
        }
    }
    render() {
        if (!this.state.login) {
            return (<Redirect to="/login" />)
        }
        else if (this.state.loading) {
            return (<div>Loading ...</div>)
        }
        else {
            return (
                <div style={{
                    backgroundImage: "url(https://catalogue.thehutgroup.com/iwoot/affiliates/files/assets/common/page-substrates/page0014.jpg)",
                    backgroundRepeat: "no-repeat",
                    backgroundSize: "cover",
                    height: "100vh"
                }}>
                    <Navbar logout={this.removeSession} />
                    <div className="main" >
                        <div className="details">
                            <h1>Heyy!</h1>
                            <h2>Here's the list of all the registerd users</h2>
                            <table>
                                <thead>
                                    <tr>
                                        <td>Id</td>
                                        <td>Email</td>
                                        <td>Name</td>
                                    </tr>
                                </thead>
                                <tbody>
                                    {this.state.data.map((ele, i) => {
                                        return (<tr key={i}>
                                            <td>{ele.id}</td>
                                            <td style={{
                                                wordBreak: 'break-all',
                                                wordWrap: 'break-word'
                                            }}>{ele.email}</td>
                                            <td style={{ textTransform: "capitalize" }}>{ele.fname} {ele.lname}</td>
                                        </tr>)
                                    })}
                                </tbody>
                            </table>
                        </div>
                        {/* <div className="Kidimage">
                            <img src={Kid} alt="kid" />
                        </div> */}
                    </div>
                </div>
            )
        }
    }
}

export default Home
