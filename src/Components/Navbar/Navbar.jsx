import React, { Component } from 'react'
import './nav.css'
export class Navbar extends Component {
    render() {
        return (
            <header>
                <div className="headerCont">
                    <h2>React Login</h2>

                    <div className="user">
                        <h2 style={{ textTransform: "capitalize" }}>{sessionStorage.getItem("name")} </h2>
                        <button className="logout" onClick={this.props.logout}>Logout</button>
                    </div>
                </div>
            </header>
        )
    }
}

export default Navbar
