import React, { Component } from "react";
import './login.css'
import axios from 'axios'
import { Baseurl } from '../utils'
import { Redirect } from "react-router";
export default class Signup extends Component {

    state = {
        fname: "",
        lname: "",
        password: "",
        confirm_password: "",
        email: "",
        errMessage: "",
        isDisabled: false,
        login: sessionStorage.getItem('token')?true:false,
        loading: false
    }
    change = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        })
    }
    submit = () => {
        this.setState({ errMessage: "", isDisabled: true })
        axios.post(Baseurl + "register", { ...this.state })
            .then(res => {
                console.log(res.data)
                if (res.data.err) {
                    this.setState({ errMessage: res.data.message, isDisabled: false })
                }
                else {
                    alert("registered successfully! Please login with this account")
                    this.setState({ login: true })
                }
            }).catch(Err => {
                console.log(Err.response)
                if (Err.response.data) {
                    this.setState({ errMessage: Err.response.data.message, isDisabled: false })
                }
                else {
                    this.setState({ errMessage: Err.message, isDisabled: false })
                }
            })
    }
    componentDidMount() {
        if (sessionStorage.getItem('token')) {
            this.setState({ loading: true })
            axios.get(Baseurl, {
                headers: {
                    'x-auth-token': sessionStorage.getItem('token')
                }
            }).then(res => {
                if (res.data.err) {
                    window.sessionStorage.removeItem("token")
                    window.sessionStorage.removeItem("name")
                    window.sessionStorage.removeItem("email")
                }
                else {
                    this.setState({ login: true })
                }
                this.setState({ loading: false })
            }).catch(Err => {
                console.log(Err)
                window.sessionStorage.removeItem("token")
                window.sessionStorage.removeItem("name")
                window.sessionStorage.removeItem("email")
                this.setState({ loading: false })
            })
        }
    }
    render() {
        if (this.state.login) {
            return (<Redirect to="/" />)
        }
        else if (this.state.loading) {
            return (<div>Loading...</div>)
        }
        else {
            return (
                <div className="form-container">
                    <div className="card">
                        <div className="head">
                            <h2>Create an account</h2>
                        </div>
                        <div className="head">
                            <p style={{ color: 'red' }}>{this.state.errMessage}</p>
                        </div>
                        <div className="form-level">
                            <label>
                                First Name:
                            </label>
                            <input type="text" name="fname" onChange={this.change} />
                        </div>
                        <div className="form-level">
                            <label>
                                Last Name:
                            </label>
                            <input type="text" name="lname" onChange={this.change} />
                        </div>
                        <div className="form-level">
                            <label>
                                Email:
                            </label>
                            <input type="email" name="email" onChange={this.change} />
                        </div>
                        <div className="form-level">
                            <label>
                                Password:
                            </label>
                            <input type="password" name="password" onChange={this.change} />
                        </div>
                        <div className="form-level">
                            <label>
                                Confirm Password:
                            </label>
                            <input type="password" name="confirm_password" onChange={this.change} />
                        </div>
                        <div className="form-level">
                            <button className={this.state.isDisabled ? "normalBtn" : "submitBtn"} onClick={this.submit} disabled={this.state.isDisabled}>Register</button>
                        </div>
                        <div className="form-level">
                            <p>Already have an account? <a href="/login">Login</a> </p>
                        </div>

                    </div>
                </div>
            );
        }

    }
}