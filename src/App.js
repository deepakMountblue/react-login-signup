import React from 'react';
import './App.css';
import { Route, Switch } from 'react-router-dom'
import Login from './Components/Login/Login';
import Home from './Components/Home/Home';
import Signup from './Components/Login/Signup'

function App() {
  
  return (
    <>
      <Switch>
        <Route path="/" exact component={Home} />
        <Route path="/login" component={Login} />
        <Route path="/signup" component={Signup} />
      </Switch>
    </>
  );
}

export default App;